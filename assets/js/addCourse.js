const addCourse=document.getElementById(`createCourse`)
const token=localStorage.getItem(`token`)

addCourse.addEventListener("submit", (e)=> {
	e.preventDefault()
	const cn=document.getElementById('courseName').value
	const desc=document.getElementById('description').value
	const price=document.getElementById('price').value

	addEventListener
	fetch(`http://localhost:3008/api/courses/createCourse`,{
		method:"POST",
		headers:{
			"Content-Type":"application/json",
			"Authorization":`Bearer ${token}`
		},
		body: JSON.stringify({
			courseName:cn,
			description:desc,
			price:price
		})
	})
	.then(result => result.json())
	.then(result => {
		console.log(result)

		if(result){
			alert(`Course successfully added`)
			window.location.replace(`./courses.html`)
		}else{
			alert(`Something went wrong`)
		}
	})
})