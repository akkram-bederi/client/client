hanconst token = localStorage.getItem(`token`)
const admin = localStorage.getItem(`isAdmin`)
const courseContainer = document.getElementById(`anyContainer`)
const buttons= document.getElementById(`buttons`)
let courses;
let cardFooter;
if(admin == "false"){
	fetch(`http://localhost:3008/api/courses/isActive`, {
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	//wait for servers reponse
	.then(result =>result.json())
	.then(result => {
		// console.log(result)
		
		//display the course using card
		if(result.length<1){
			return `No active courses`
		}else{
			courses = result.map(course =>{
				console.log(course)
				const {
					courseName,
					description,
					price,
					isOffered,
					_id
				}=course

				return(
					`
						<div class="col-12 col-md-4 m-5">
							<div class="card" style="width: 18rem">
							  <div class="card-body">
							    <h5 class="card-title">${courseName}</h5>
							    <p class="card-text">${description}</p>
							    <p class="card-text">${price}</p>
							    <a class="btn btn-info" href="./specificCourse.html?courseId=">Select Course</a>
							  </div>
							</div>
						</div>
					`
				)
			})
		}
		courseContainer.innerHTML = courses
	})
} else{

	//admin

	//sohw the button if it is a Admin

	buttons.innerHTML= 
		`
		<a class="btn btn-info m-3" href="./addCourse.html">Add Course</a>
		<a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>
		` 

	fetch(`http://localhost:3008/api/courses/`, {
		method:"GET",
		headers:{
			"Authorization":`Bearer ${token}`
		}
	})
	//wait for servers reponse
	.then(result =>result.json())
	.then(result => {
		// console.log(result)
		
		//display the course using card
		if(result.length<1){
			return `No active courses`
		}else{
			courses = result.map(course =>{
				console.log(course)
				const {
					courseName,
					description,
					price,
					isOffered,
					_id
				}=course

				if(isOffered){
					//if course is offered
					cardFooter=
					`
					<a class="btn btn-info btn-block" href="./editCourse.html?courseId=${_id}">Edit Course</a>
					<a class="btn btn-danger btn-block" href="./archive.html?courseId=${_id}">Archive</a>
					<a class="btn btn-secondary btn-block" href="./delete.html?courseId=${_id}">Delete</a>
					`
				}else{
					//if course is not offered
					cardFooter = 
					`
					<a class="btn btn-info btn-block" href="./editCourse.html?courseId=${_id}">Edit Course</a>
					<a class="btn btn-danger btn-block" href="./unarchive.html?courseId=${_id}">Unarchive</a>
					<a class="btn btn-secondary btn-block" href="./delete.html?courseId=${_id}">Delete</a>
					`

				}

				return(
					`
					<div class="col-12 col-md-3 m-5">
						<div class="card" style="width: 18rem">
						  <div class="card-body">
						    <h5 class="card-title">${courseName}</h5>
						    <p class="card-text">${description}</p>
						    <p class="card-text">${price}</p>
						  </div>
						  <div class="card-footer">
						  ${cardFooter}
						  </div>
						</div>
					</div>
					`
				)
			})
		}
		courseContainer.innerHTML = courses
	})
	courseContainer.innerHTML = courses

}