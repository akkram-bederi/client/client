
let params=new URLSearchParams(document.location.search)
const courseId=params.get("courseId")
const token=localStorage.getItem(`token`)

const cn=document.getElementById('courseName')
const desc=document.getElementById(`description`)
const price = document.getElementById(`price`)

fetch(`http://localhost:3008/api/courses/${courseId}`, {
	method:"GET",
	headers:{
		"Authorization":`Bearer ${token}`
	}
}).then(result => result.json())
.then(result => {
	console.log(result)

	cn.value=result.courseName
	desc.value=result.description 
	price.value = result.price
})

const editForm = document.getElementById(`editCourse`)
editForm.addEventListener(`submit`,(e)=> {
	e.preventDefault()

	fetch(`http://localhost:3008/api/courses/${courseId}/updateCourse`, {
		method:"PATCH",
		headers:{
			"Content-Type":"application/json",
			"Authorization":`Bearer ${token}`
		},
		body: JSON.stringify({	
			courseName:cn.value, 
			description:desc.value,
			price:price.value
		})
	})
	.then(result => result.json())
	.then(result => {
		console.log(result)

		if(result){
			alert(`Course successfulty updated!`)
			window.location.replace(`./courses.html`)
		}else{
			alert(`Something went wrong`)
		}
	})
})